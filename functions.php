<?php
    error_reporting(E_ALL & E_NOTICE);

    //create a log script and save it to log.txt file with every phase
function logaction($log_msg){
    $log_filename = "log";

    if (!file_exists($log_filename)){
        mkdir($log_filename, 0777, true);
    }
    $log_filename_data = $log_filename . '/log_' .date('d-M-Y') . '.log';
    file_put_contents($log_filename_data, date('H:i:s  ') . $log_msg . "\n", FILE_APPEND);
}

    // create a new directory in local project path
function directorycreate($name){
    $path = getcwd();
    $dir = $path . '/' . $name;

    if (!file_exists($dir)){
        return mkdir($dir, 0777, true);
    }
}

    // replace spaces with dashes " something to be    " => "something_to_be"
function sanitize($string){
    $str = trim($string, " \t\n\r\0\x0B");
    return preg_replace('/[\s_\W]/', '_', $str);
}

    // current date
function cdate(){
    $now = new DateTime();
    return $now->format('Y-m-d H:i:s');
    //  $now->getTimestamp();
}

    //grab the page and save the content to local
function savePage($link, $name, $location){
    $dom = new DOMDocument();
    $dom->loadHTMLFile($link);
    $path = $location . '/' . $name . '/' . $name . '.html';

    $dom->saveHTMLFile($path);
    return $path;
}

    // grab the downloaded link from the url
function getUrl($link){
    $dom = new DOMDocument();
    libxml_use_internal_errors(true);
    $dom->loadHTMLFile($link);

    $xpath = new DOMXPath($dom);
    $hyperlinks = $xpath->query('//a[@class="c-button c-button--blue c-button__icon-right test-download-book-options test-bookpdf-link"]');

    $hyperlink = $hyperlinks[1]->getAttribute('href');

    // retun the link as /content/pdf/10.1007%2Fb100747.pdf
    return $hyperlink;
}

// identify the site as www.some-site.com
function getSite($link){
    $patern = "/((http|ftp|https)?(:\/\/)?([a-zA-Z0-9-.]+))/";
    $site = preg_match_all($patern, $link, $matches);

    return $matches[0][0];
}