<html>
<H4>Please choose you file!!!</H4>
    <div class="">
        <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data">
            <label for="myfile">Select a file:</label>
            <input type="file" id="file" name="file"><br><br>
            <input type="submit" name="submit">
        </form>

    </div>

</html>


<?php
require_once('functions.php');

if (isset($_POST["submit"])) {
    echo 'submit is performed!';
    if (isset($_FILES["file"])) {

        //if there was a error uploading the file
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " .$_FILES["file"]["error"] . "<br />";
        }
        else {
            // Print file details
            echo "Upload: " . $_FILES["file"]["name"] . "<br />";
            echo "File Type: " . $_FILES["file"]["type"] . "<br />";
            echo "File Size: " . round(($_FILES["file"]["size"] / 1024), 2) . " Kb <br />";
            echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />" ;

            //if file already exists
            if (file_exists("/upload/" . $_FILES["file"]["name"])){
                echo $_FILES["file"]["name"] . " already exists, please choose another one!";
            }
            else {
                //store file in directory "upload" with the name of "uploaded_file.csv"
                $storagename = "upload_file.csv";
                move_uploaded_file($_FILES["file"]["tmp_name"], getcwd()."/upload/" . $storagename);
                echo "Store in: " . "/upload/" . $_FILES["file"]["name"] . cdate() . "<br>";
            }
        }
        } else {
            echo "No file selected";
        }
}

$uploadedfile = $_FILES["file"]["name"];

if (($handle = fopen($uploadedfile, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $arraycsv[] = Array('id'=>$data['0'], 'Bookname'=>$data['1'], 'href'=>$data['2']);
       // logaction("am creat: 'id:'=> " . $data['0'] . "'Bookname'=>" . $data['1'] . "'href'=>" . $data['2'] );
    }
}

for ($x = 0; $x <= count($arraycsv); $x++ ){
    $name = sanitize($arraycsv[$x]['Bookname']);
    $link = $arraycsv[$x]['href'];
    $location = getcwd();

    // download and save the htmll page so we can parse it (every page in one single directory)
    if ( !file_exists($name)){
        // create new directory
        // save new .html file in created directory
        logaction('directory was created: ' . directorycreate($name));
        $pageContent = savePage($link, $name, $location);
        logaction('.html page was downloaded: ' . $pageContent);

        // parse the .html file and grab only the .pdf link of the downloadable document (ex. /content/pdf/10.1007%2Fb100747.pdf )
        $url = getUrl($pageContent);
        logaction('we use: ' . $url);

        // create correct link to download the book (ex. string(58) "http://link.springer.com/content/pdf/10.1007%2Fb100747.pdf")
        $dlink = getSite($link) . $url;
        logaction('complete download link is: ' . $dlink);

        sleep(5);

        // Use file_get_contents() function to get the file
        // from url and use file_put_contents() function to
        // save the file by using base name
        if(file_put_contents( $name, file_get_contents($dlink))) {
            logaction($name);
            logaction("Book: ". $name . " was downloaded! ");
            die();
        }
        else {
            logaction("Book: ". $name . " was corupted! ");
        }


        // wait 5 seconds after download is finished and continue the loop
    } else {
        // return file and/or directory already in use
        print_r( 'create directory' );
    }
}
